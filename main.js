//menu fixed top

$(function () {
    $(document).scroll(function () {
        var $nav = $(".fixed-top");
        $nav.toggleClass('scrolled', $(this).scrollTop() > $nav.height());
    });
  });


  // funkcija za scroll na tablet 

  $("#up").on("click", function(){
    // $(".inner-tablet").animate({scrollTop:$(".inner-tablet").height()}, 'slow');
    $(".tablet-image").css("transform", "translateY(0px)");
     $(".tablet-image").css("transition", "all .4s");

    });  

  $("#down").on("click", function(){
    // $(".inner-tablet").animate({scrollTop:$(".inner-tablet").height()}, 'slow');
    $(".tablet-image").css("transform", "translateY(-360px)");
     $(".tablet-image").css("transition", "all .4s");
   
  });  


 // clock
  var date = new Date;
  var seconds = date.getSeconds();
  var minutes = date.getMinutes();
  var hours = date.getHours(); 

  var hands = [
    {
      hand: 'hours',
      angle: (hours * 30) + (minutes / 2)
    },
    {
      hand: 'minutes',
      angle: (minutes * 6)
    },
    {
      hand: 'seconds',
      angle: (seconds * 6)
    }
  ];
  for (var j = 0; j < hands.length; j++) {
    var elements = document.querySelectorAll('.' + hands[j].hand);
    for (var k = 0; k < elements.length; k++) {
        elements[k].style.webkitTransform = 'rotateZ('+ hands[j].angle +'deg)';
        elements[k].style.transform = 'rotateZ('+ hands[j].angle +'deg)';
        // If this is a minute hand, note the seconds position (to calculate minute position later)
        if (hands[j].hand === 'minutes') {
          elements[k].parentNode.setAttribute('data-second-angle', hands[j + 1].angle);
        }
    }
}

function setUpMinuteHands() {
  // Find out how far into the minute we are
  var containers = document.querySelectorAll('.minutes-container');
  var secondAngle = containers[0].getAttribute("data-second-angle");
  if (secondAngle > 0) {
    // Set a timeout until the end of the current minute, to move the hand
    var delay = (((360 - secondAngle) / 6) + 0.1) * 1000;
    setTimeout(function() {
      moveMinuteHands(containers);
    }, delay);
  }
}

function moveMinuteHands(containers) {
  for (var i = 0; i < containers.length; i++) {
    containers[i].style.webkitTransform = 'rotateZ(6deg)';
    containers[i].style.transform = 'rotateZ(6deg)';
  }
  // Then continue with a 60 second interval
  setInterval(function() {
    for (var i = 0; i < containers.length; i++) {
      if (containers[i].angle === undefined) {
        containers[i].angle = 12;
      } else {
        containers[i].angle += 6;
      }
      containers[i].style.webkitTransform = 'rotateZ('+ containers[i].angle +'deg)';
      containers[i].style.transform = 'rotateZ('+ containers[i].angle +'deg)';
    }
  }, 60000);
}

function moveSecondHands() {
  var containers = document.querySelectorAll('.seconds-container');
  setInterval(function() {
    for (var i = 0; i < containers.length; i++) {
      if (containers[i].angle === undefined) {
        containers[i].angle = 6;
      } else {
        containers[i].angle += 6;
      }
      containers[i].style.webkitTransform = 'rotateZ('+ containers[i].angle +'deg)';
      containers[i].style.transform = 'rotateZ('+ containers[i].angle +'deg)';
    }
  }, 1000);
}


// fade efext
//*************************************************** */

$(window).on("load",function() {
  $(window).scroll(function() {
    var windowBottom = $(this).scrollTop() + $(this).innerHeight();
    $(".fade").each(function() {
      /* Check the location of each desired element */
      var objectBottom = $(this).offset().top + $(this).outerHeight();
      
      /* If the element is completely within bounds of the window, fade it in */
      if (objectBottom < windowBottom) { //object comes into view (scrolling down)
        if ($(this).css("opacity")==0) {$(this).fadeTo(500,1);}
      } else { //object goes out of view (scrolling up)
        if ($(this).css("opacity")==1) {$(this).fadeTo(500,0);}
      }
    });
  }).scroll(); //invoke scroll-handler on page-load
});



//collapse plus click

$('.1').on('click', function () {
  $('#collapseOne').toggleClass('show');
   $("#collapseTwo,#collapseThree,#collapseFour").removeClass('show');
})
$('.2').on('click', function () {
  $('#collapseTwo').toggleClass('show');
  $("#collapseOne,#collapseThree,#collapseFour").removeClass('show');
})
$('.3').on('click', function () {
  $('#collapseThree').toggleClass('show');
  $("#collapseTwo,#collapseOne,#collapseFour").removeClass('show');
})
$('.4').on('click', function () {
  $('#collapseFour').toggleClass('show');
  $("#collapseTwo,#collapseThree,#collapseOne").removeClass('show');
})

//service click




$('#coding').on('click', function () {
  $('.listCoding').addClass('show2');
   $(".listBranding,.listMarketing").removeClass('show2');

        $(".listCoding").animate({
          left: '250px'});
  
})
$('#marketing').on('click', function () {
  $('.listMarketing').addClass('show2');
  $(".listCoding,.listBranding").removeClass('show2');

  $(".listMarketing").animate({
          left: '250px'});
})
$('#branding').on('click', function () {
  $('.listBranding').addClass('show2');
  $(".listCoding,.listMarketing").removeClass('show2');

  $(".listBranding").animate({
          left: '250px'});
})





//lightbox

function openModal() {
  document.getElementById('myModal').style.display = "block";
}

function closeModal() {
  document.getElementById('myModal').style.display = "none";
}

var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("demo");
  var captionText = document.getElementById("caption");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
  captionText.innerHTML = dots[slideIndex-1].alt;
}



//colapse
$('#myCollapse').on('shown.bs.collapse', function (e) {
  // Action to execute once the collapsible area is expanded
})

$('.collapse').collapse();

$('#myCollapsible').collapse({
  toggle: false
})



// tryouts 

$(function(){  // $(document).ready shorthand
  $('#monster').fadeIn('slow');
});
$(document).ready(function() {
    
  /* Every time the window is scrolled ... */
  $(window).scroll( function(){
  
      /* Check the location of each desired element */
      $('.hideme').each( function(i){
          
          var bottom_of_object = $(this).position().top + $(this).outerHeight();
          var bottom_of_window = $(window).scrollTop() + $(window).height();
          
          /* If the object is completely visible in the window, fade it it */
          if( bottom_of_window > bottom_of_object ){
              
              $(this).animate({'opacity':'1'},2500);
                  
          }
          
      }); 
  
  });
  
});

// Work carousel page

